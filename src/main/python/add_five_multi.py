#-----------------------------------
# Add five to the input parameter
# Return the result and the input parameter
#-----------------------------------

from time import sleep
import sys

def returnResult(inputNumber):
	return int(inputNumber) + 5

def isInteger(myvar):
    try:
        int(myvar)
        return True
    except ValueError:
        return False

def main():
	result = ats_parameter(0)

	ats_log(f"Iteration: {ats_iteration}/{ats_iterations_count}")
	ats_comment(f"Iteration: {ats_iteration}/{ats_iterations_count}")
		
	if (isInteger(ats_parameter(0))):
		result = returnResult(ats_parameter(0))

		ats_log(f"Resultat du calcul: {result}")
		ats_comment(f"Resultat du calcul: {result}")

		ats_return([result, ats_parameter(0)])
	else:
		ats_return(["Erreur : La valeur entrée n'est pas un entier", ats_parameter(0)])
	
main()