# ------------------------------------------------------------------------------
# Get parameter passed by the calling script
# ------------------------------------------------------------------------------
# ex. param0 = ats_parameter(0)
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Send console output logs
# ------------------------------------------------------------------------------
# ex. ats_log("Ats output log ...")
# ------------------------------------------------------------------------------

# This is an example of Python script, you can delete following lines ...

from time import sleep
import sys
import re

def calculate_nb_word(paragraphe):
    mots = re.split(r'\s+', paragraphe.strip())
    return len(mots)

def main():
	ats_log("Start ATS Python subscript")
	ats_log(sys.version)

	paragraphe = ats_parameter(0)
	print('TEXT:', paragraphe)

	# Calculer le nombre de mots
	nb_words = calculate_nb_word(paragraphe)
	print('Nb words in paragraph:', nb_words)

	ats_return(nb_words)
    
main()

# ------------------------------------------------------------------------------
# Return values to the calling script
# ------------------------------------------------------------------------------
# ex. ats_return ('resultvalue')
# or ats_return (['resultvalue1','resultvalue2', ...])
# ------------------------------------------------------------------------------