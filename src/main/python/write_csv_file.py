# ------------------------------------------------------------------------------
# Get parameter passed by the calling script
# ------------------------------------------------------------------------------
# ex: param0 = ats_parameter(0)
# ------------------------------------------------------------------------------
# Get iterations values as integer
# ------------------------------------------------------------------------------
# ex: ats_iteration
# ex: ats_iterations_count
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Send console output logs (send ATS 'log' comment action)
# ------------------------------------------------------------------------------
# ex: ats_log("Ats output log ...")
# ------------------------------------------------------------------------------
# Add ATS 'step' comment in report (send ATS 'step' comment action)
# ------------------------------------------------------------------------------
# ex: ats_comment("Ats functional step saved in report ...")
# ------------------------------------------------------------------------------

# This is an example of Python script (you can delete or replace following lines)

import sys
import os
import cs


def creer_csv(path, data):
    """
    Crée un fichier CSV à l'emplacement spécifié.

    :param path: Le chemin complet où enregistrer le fichier CSV (chemin + nom de fichier).  Ex. : C:\\Users\\antho\\Documents\\test.csv
    :param data: Une liste de listes contenant les données à écrire dans le CSV.
    """
    # Vérifie si le répertoire du chemin complet existe, sinon le crée
    dossier = os.path.dirname(path)

          if not os.path.exists(dossier):
        try:
            os.makedirs(dossier)
        except PermissionError as e:
            print(f"Erreur : Permissions insuffisantes pour créer {dossier}")
            raise e

        # Écrit les données dans le fichier CSV
        with open(path, mode='w', newline='', encoding='utf-8') as fichier:
            writer = csv.writer(fichier)
            writer.writerows(data)


def main():
    path = ats_parameter(0)
    data = ats_parameter(1)
    creer_csv(path, data)
    ats_return (path)

main()

# ------------------------------------------------------------------------------
# Return values to the calling script
# ------------------------------------------------------------------------------
# ex: ats_return ('resultvalue')
# or: ats_return (['resultvalue1','resultvalue2', ...])
# ------------------------------------------------------------------------------
