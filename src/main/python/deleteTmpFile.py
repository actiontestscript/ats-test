from time import sleep
import sys
import os
import tempfile

def main():
	filepath = os.path.join(tempfile.gettempdir(), ats_parameter(0))
	# Delete file
	try:
		os.remove(filepath)
		print(f"Le fichier '{filepath}' a été supprimé avec succès.")
	except FileNotFoundError:
		print(f"Erreur : Le fichier '{filepath}' n'existe pas ou a déjà été supprimé.")
	except Exception as e:
		print(f"Erreur lors de la suppression du fichier : {e}")
    
main()

# ===============================================================================
# Return values to the calling script (single or array value)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ats_return ('resultvalue')
# ats_return (['resultvalue1','resultvalue2', 42, True, ...])
# ===============================================================================