# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# Ecrit un fichier contenant 5 chiffres dans le repertoire temporaire du système 
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

from time import sleep
import sys
import tempfile
import os

def writeRandomIntFile(fileName):
    with open(fileName, 'w') as fichier:
        fichier.write("54\n")
        fichier.write("6\n")
        fichier.write("76\n")
        fichier.write("62\n")
        fichier.write("83\n")

def main():
	ats_log("Start ATS Python subscript")
	ats_log(sys.version)

	PathTmp = tempfile.gettempdir()
	PathFileTmp = os.path.join(PathTmp, "listOfInt.txt")
	writeRandomIntFile(PathFileTmp);

	print(f"Chemin : {PathFileTmp}")
    
main()

# ===============================================================================
# Return values to the calling script (single or array value)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ats_return ('resultvalue')
# ats_return (['resultvalue1','resultvalue2', 42, True, ...])
# ===============================================================================