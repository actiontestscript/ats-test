# ------------------------------------------------------------------------------
# Get parameter passed by the calling script
# ------------------------------------------------------------------------------
# ex. param0 = ats_parameter(0)
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Send console output logs
# ------------------------------------------------------------------------------
# ex. ats_log("Ats output log ...")
# ------------------------------------------------------------------------------

# This is an example of Python script, you can delete following lines ...

from time import sleep
import sys

def returnTxt():
	return "ATS PYTHON RETURN"

def main():
	ats_log("Start ATS Python subscript")
	ats_log(sys.version)
	ats_return(returnTxt())
    
main()

# ------------------------------------------------------------------------------
# Return values to the calling script
# ------------------------------------------------------------------------------
# ex. ats_return ('resultvalue')
# or ats_return (['resultvalue1','resultvalue2', ...])
# ------------------------------------------------------------------------------