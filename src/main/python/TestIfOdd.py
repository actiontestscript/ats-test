#-------------------------------------------------------------------------------------
# Lit le fichier dans \temp contenant des entiers et rend le nombre de chiffres pairs
# Supprime le fichier à la fin
#------------------------------------------------------------------------------------- 

from time import sleep
import sys
import tempfile
import os

def readFileAndTestOdd(fileName):
    count_even_numbers = 0  # Compteur pour le nombre de nombres pairs

    try:
        with open(fileName, 'r') as fichier:
            lines = fichier.readlines()

            for i, line in enumerate(lines, start=1):
                myNumber = int(line.strip())  # Supprimer les espaces blancs et convertir en entier
                if myNumber % 2 != 0:
                    print(f"line {i}: {myNumber} est impair.")
                else:
                    print(f"line {i}: {myNumber} est pair.")
                    count_even_numbers += 1  # Incrémenter le compteur pour chaque nombre pair

    except FileNotFoundError:
        print(f"Erreur : Le fichier '{fileName}' n'existe pas.")
    except ValueError:
        print("Erreur : Impossible de convertir une ligne en entier.")

    return count_even_numbers  # Retourner le nombre de nombres pairs


def main():
	ats_log("Start ATS Python subscript")
	ats_log(sys.version)

	PathTmp = tempfile.gettempdir()
	PathFileTmp = os.path.join(PathTmp, "listOfInt.txt")
	nombre_pairs = readFileAndTestOdd(PathFileTmp)

	# Delete file
	try:
		os.remove(PathFileTmp)
		print(f"Le fichier '{PathFileTmp}' a été supprimé avec succès.")
	except FileNotFoundError:
		print(f"Erreur : Le fichier '{PathFileTmp}' n'existe pas ou a déjà été supprimé.")
	except Exception as e:
		print(f"Erreur lors de la suppression du fichier : {e}")

	ats_return(nombre_pairs)
    
main()