from time import sleep
import sys
import os
import tempfile

def write_to_file(filepath, entry):
    try:
        with open(filepath, 'a') as file:  # 'a' pour append (ajouter à la fin du fichier)
            file.write(entry + '\n')
        print(f"Entry successfully added to the file: {filepath}")
    except Exception as e:
        print(f"Error while writing to the file: {e}")

def main():
	ats_log("Start ATS Python subscript")
	ats_log(sys.version)

	filepath = os.path.join(tempfile.gettempdir(), "tmpData.txt")
	entry = ats_parameter(0)	#parameter 1, entry ti write in the file
	write_to_file(filepath, entry)

	#ats_return (filepath)
	ats_return ("tmpData.txt")

main()