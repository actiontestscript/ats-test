# ===============================================================================
# Get parameter passed by the calling script
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# param0 = ats_parameter(0)
# ===============================================================================
# Get iteration values as integer
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ats_iteration
# ats_iterations_count
# ===============================================================================
# Send console output logs (send ATS 'log' comment action)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ats_log("Ats output log ...")
# ===============================================================================
# Add ATS 'step' comment in report (send ATS 'step' comment action)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ats_comment("Ats functional step saved in report ...")
# ===============================================================================

# This is an example of Python script (you can delete or replace following lines)

from time import sleep
import sys

def main():
	ats_log("Start ATS Python subscript")
	ats_log(sys.version)

	myNumber = int(ats_parameter(0).strip())  # Supprimer les espaces blancs et convertir en entier
	ats_return (myNumber % 2 == 0)
    
main()

# ===============================================================================
# Return values to the calling script (single or array value)
# - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
# ats_return ('resultvalue')
# ats_return (['resultvalue1','resultvalue2', 42, True, ...])
# ===============================================================================