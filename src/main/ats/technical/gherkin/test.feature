#Author: Not defined
#Assignee: 

Feature: Support_global_variables_from_execution_profile...




  
#Author:   Last Updated: 
#URL: https://berger-levrault.atlassian.net/projects/ATM?selectedItem=com.atlassian.plugins.atlassian-connect-plugin:com.kanoah.test-manager__main-project-page#!/testCase/ATM-T177
   @TestCaseKey=ATM-T177 @External @ScenarioRecord=Commons_KDC/Login_KDC_protectedVariables_Declare 
   Scenario Outline: Login_KDC_protectedVariables_Declare Login_KDC_protectedVariables_Declare 
   
Given  je suis connecté sur le portail application KDC ATM-T177L11
Then  je vérifie la page application KDC ATM-T177L12

       
      
#Author:   Last Updated: 
#URL: https://berger-levrault.atlassian.net/projects/ATM?selectedItem=com.atlassian.plugins.atlassian-connect-plugin:com.kanoah.test-manager__main-project-page#!/testCase/ATM-T178
   @TestCaseKey=ATM-T178 @External @ScenarioRecord=Commons_KDC/MakeAppointment_KDC_protectedVariables_Declare @RelatedTo=ATM-T177 
   Scenario Outline: MakeAppointment_KDC_protectedVariables_Declare MakeAppointment_KDC_protectedVariables_Declare 
   
Given  i am on the page KDC Make Appointment ATM-T178L19
When  je clique le bouton KDC Make Appointment ATM-T178L20
Then  je vérifie la page KDC Authentification ATM-T178L21

       
      
#Author:   Last Updated: 
#URL: https://berger-levrault.atlassian.net/projects/ATM?selectedItem=com.atlassian.plugins.atlassian-connect-plugin:com.kanoah.test-manager__main-project-page#!/testCase/ATM-T179
   @TestCaseKey=ATM-T179 @External @ScenarioRecord=Commons_KDC/Authenticate_KDC_protectedVariables_Declare @RelatedTo=ATM-T178 
   Scenario Outline: Authenticate_KDC_protectedVariables_Declare Authenticate_KDC_protectedVariables_Declare 
   
Given  je suis sur la page authentification de KDC ATM-T179L28
And  je renseigne mon identifiant "identifiant" de KDC ATM-T179L29
And  je renseigne mon password "password" de KDC ATM-T179L30
When  je clique le bouton Login de KDC ATM-T179L31
Then  je vérifie la page Make Appointment de KDC ATM-T179L32

       
      
#Author:   Last Updated: 
#URL: https://berger-levrault.atlassian.net/projects/ATM?selectedItem=com.atlassian.plugins.atlassian-connect-plugin:com.kanoah.test-manager__main-project-page#!/testCase/ATM-T181
   @TestCaseKey=ATM-T181 @External @ScenarioRecord=Commons_KDC/Login_KDC_site_Declare_GlobalVariable 
   Scenario Outline: Login_KDC_site_Declare_GlobalVariable Login_KDC_site_Declare_GlobalVariable 
   
Given  je suis connecté sur le portail application KDC ATM-T181L39
And  je clique le bouton "Make Appointment" ATM-T181L40
And  je stocke dans la globalVariable la valeur de "demoPassword" ATM-T181L41
When  je renseigne valeur de "GlobalVariable.JohnDoe" dans le champ "Username" ATM-T181L42
And  je renseigne valeur de "GlobalVariable.ThisIsNotAPassword" dans le champ "Password" ATM-T181L43
And  je clique le bouton "Login" ATM-T181L44
Then  je vérifie la présence de "Appointment" ATM-T181L45

       
      
#Author:   Last Updated: 
#URL: https://berger-levrault.atlassian.net/projects/ATM?selectedItem=com.atlassian.plugins.atlassian-connect-plugin:com.kanoah.test-manager__main-project-page#!/testCase/ATM-T183
   @TestCaseKey=ATM-T183 @External @ScenarioRecord=Commons_KDC/Login_KDC_site_Declare_GlobalVariable_By_GlobalVariable @TestcaseKey=ATM-T183 
   Scenario Outline: Login_KDC_site_Declare_GlobalVariable_By_GlobalVariable Login_KDC_site_Declare_GlobalVariable_By_GlobalVariable 
   
Given  je suis connecté sur le portail application KDC ATM-T183L52
And  je clique le bouton "Make Appointment" et affecte globalVariable.JohnDoeTGO à la globalVariable.JohnDoe ATM-T183L53
When  je renseigne "globalVariable.JohnDoeTGO" dans le champ "Username" ATM-T183L54
And  je renseigne "ThisIsNotAPassword" dans le champ "Password" ATM-T183L55
And  je clique le bouton "Login" ATM-T183L56
Then  je vérifie la présence de "Appointment" ATM-T183L57

Examples:
   | Portal       | Button          | PasswordField     | Username                 | Password                     | ConfirmationText |
   | ATM-T181L39  | Make Appointment| demoPassword      | GlobalVariable.JohnDoe    | GlobalVariable.ThisIsNotAPassword | Appointment      |
   | ATM-T181L40  | Schedule Visit  | anotherPassword   | GlobalVariable.JaneDoe    | GlobalVariable.AnotherPassword   | Visit Confirmed  |       
      

