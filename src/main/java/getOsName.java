import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class getOsName extends ActionTestScript{

	/**
	 * Test Name : <b>getOsName</b>
	 * Test Author : <b>Grante Eric</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	@Test
	public void testMain(){
		// -----------------------------------------------
		// Get parameters passed by the calling script :
		// getParameter(int index)
		// -----------------------------------------------
		// String param0 = getParameter(0).toString();
		// int param0 = getParameter(0).toInt();
		// double param0 = getParameter(0).toDouble();
		// boolean param0 = getParameter(0).toBoolean();
		// -----------------------------------------------
		// int it = getIteration(); -> return current iteration loop
		// String path = getCsvFilePath(); -> return csv file path sent as parameter to call current script
		// File file = getCsvFile(); -> return csv file sent as parameter to call current script
		// File file = getAssetsFile("[relative path string]"); -> return a file in the project's 'assets' folder
		// String url = getAssetsUrl("[relative path string]"); -> return url path of a file in the project's 'assets' folder
		
		String sysOsName = getParameter(0).toString();
		Pattern pattern = Pattern.compile("(Microsoft|Linux|MacOS)");
		Matcher matcher = pattern.matcher(sysOsName);

		String result="";
		if(matcher.find()) {
		     String matchStr = matcher.group().toLowerCase();
		     switch(matchStr){
		          case "microsoft" : 
		               result = "Microsoft";
		          break;
		          case "linux" :
		               result = "Linux";
		          break;
		          case "macos" :
		               result = "MacOs";
		          break;
		     }
		}
		
		returnValues(result);	
		
		
		// -----------------------------------------------
		// Return string values to calling script :
		// returnValues(String ...)
		// -----------------------------------------------
		// returnValues("value", stringVariable);
	}
}