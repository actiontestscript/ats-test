import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;

public class double_return extends ActionTestScript{

	/**
	 * Test Name : <b>SingleCallDoubleReturn</b>
	 * Test Author : <b>agili</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	@Test
	public void testMain(){
		returnValues("Premier retour", "Deuxiéme retour");
	}
}