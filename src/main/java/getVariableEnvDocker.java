import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class getVariableEnvDocker extends ActionTestScript{

	/**
	 * Test Name : <b>getVariableEnvDocker</b>
	 * Test Author : <b>Grante Eric</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	public Boolean checkVersion(String version){
	     int major, minor, patch, build;
	     major = minor = patch = build = 0;
	     String[] versionParts = version.split("\\.");
	     if(versionParts.length >0) major = Integer.parseInt(versionParts[0]);
	     if(versionParts.length >1) minor= Integer.parseInt(versionParts[1]);
	     if(versionParts.length >2) patch = Integer.parseInt(versionParts[2]);
	     if(versionParts.length >3) build = Integer.parseInt(versionParts[3]);
	     if(major > 1) return true;
	     else if(major == 1 && minor >=2) return true;
	     else return false;
	}

	@Test
	public void testMain(){
		// -----------------------------------------------
		// Get parameters passed by the calling script :
		// getParameter(int index)
		// -----------------------------------------------
		// String param0 = getParameter(0).toString();
		// int param0 = getParameter(0).toInt();
		// double param0 = getParameter(0).toDouble();
		// boolean param0 = getParameter(0).toBoolean();
		// -----------------------------------------------
		// int it = getIteration(); -> return current iteration loop
		// String path = getCsvFilePath(); -> return csv file path sent as parameter to call current script
		// File file = getCsvFile(); -> return csv file sent as parameter to call current script
		// File file = getAssetsFile("[relative path string]"); -> return a file in the project's 'assets' folder
		// String url = getAssetsUrl("[relative path string]"); -> return url path of a file in the project's 'assets' folder
		String systemName = getParameter(0).toString();
		String version = getParameter(1).toString();
		Pattern pattern = Pattern.compile("linux", Pattern.CASE_INSENSITIVE);  
		Matcher matcher = pattern.matcher(systemName);


		String dockerValue = System.getenv("DOCKER");		
		if (dockerValue != null && matcher.find() && checkVersion(version) ) returnValues(1);
		else returnValues(0);
		
		
		
		// -----------------------------------------------
		// Return string values to calling script :
		// returnValues(String ...)
		// -----------------------------------------------
		// returnValues("value", stringVariable);
	}
}     