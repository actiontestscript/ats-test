import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;

public class calculate_nb_words_multi extends ActionTestScript{

	/**
	 * Test Name : <b>CalculateNbWords</b>
	 * Test Author : <b>agili</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	static int calculateNbWord(String paragraphe) {
        	String[] mots = paragraphe.trim().split("\\s+");
	        return mots.length;
    	}

	@Test
	public void testMain(){
		int it = getIteration();
		String path = getCsvFilePath();
		String url = getAssetsUrl("[relative path string]");

		String paragraphe = getParameter(0).toString();
		int nbWords = calculateNbWord(paragraphe);
		returnValues(String.valueOf(nbWords), paragraphe, String.valueOf(it));
	}
}