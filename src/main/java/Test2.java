import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;

public class Test2 extends ActionTestScript{

	/**
	 * Test Name : <b>Test</b>
	 * Test Author : <b>analie.garcia</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	@Test
	public void testMain(){

	
	String param0 = getParameter(0).toString();
		returnValues(param0);
	}
}