package mustfailed;

import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;

public class ArrayIndexOutOfBoundsException extends ActionTestScript{

	/**
	 * Test Name : <b>ArrayIndexOutOfBoundsException</b>
	 * Test Author : <b>Anthony</b>
	 * Test Description : <i></i>
	 * Test Prerequisites : <i></i>
	 */

	@Test
	public void testMain(){
		int[] data = {1, 2, 3};
                int element = data[3]; 
	}
}