import org.testng.annotations.Test;
import org.openqa.selenium.Keys;
import com.ats.executor.ActionTestScript;
import com.ats.script.actions.*;
import com.ats.generator.objects.Cartesian;
import com.ats.generator.objects.mouse.Mouse;
import com.ats.tools.Operators;
import com.ats.generator.variables.Variable;

public class add_five extends ActionTestScript {

    /**
     * Test Name : <b>AddFive</b>
     * Test Author : <b>agili</b>
     * Test Description : <i></i>
     * Test Prerequisites : <i></i>
     */

    // Méthode pour vérifier si la valeur est un entier
    public static boolean isInteger(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static int addFive(int input) {
	return input + 5;
    }

    @Test
    public void testMain() {
 	if (isInteger(getParameter(0).toString())) {
		int result = addFive(getParameter(0).toInt());
 		returnValues(result);
	} else {
		returnValues("Erreur : La valeur entrée n'est pas un entier");
	}

    }
}

