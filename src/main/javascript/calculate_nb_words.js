// ------------------------------------------------------------------------------
// Get parameter passed by the calling script
// ------------------------------------------------------------------------------
// ex. var param0 = ats_parameter(0);
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// Use 'ats_element' to get the web element passed by the calling script
// ------------------------------------------------------------------------------
// ex. ats_element.style.visibility = 'hidden';
// ------------------------------------------------------------------------------

console.log('Javascript code launched from ATS execution ...');

// ------------------------------------------------------------------------------
// Return values to the calling script
// ------------------------------------------------------------------------------
// ex. ats_return ('resultvalue');
// or ats_return (['resultvalue1','resultvalue2', ...]);
// ------------------------------------------------------------------------------

function CalculateNbWord(paragraphe) {
    let mots = paragraphe.trim().split(/\s+/);
    return mots.length;
}

let paragraphe = ats_parameter(0);
console.log('TEXT: ' + paragraphe);
let nbWords = CalculateNbWord(paragraphe);
console.log('Nb words in paragraph : ' + nbWords);

ats_return(nbWords);