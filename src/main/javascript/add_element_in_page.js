// ------------------------------------------------------------------------------
// Get parameter passed by the calling script
// ------------------------------------------------------------------------------
// ex. var param0 = ats_parameter(0);
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// Use 'ats_element' to get the web element passed by the calling script
// ------------------------------------------------------------------------------
// ex. ats_element.style.visibility = 'hidden';
// ------------------------------------------------------------------------------

console.log('Javascript code launched from ATS execution ...');

// ------------------------------------------------------------------------------
// Return values to the calling script
// ------------------------------------------------------------------------------
// ex. ats_return ('resultvalue');
// or ats_return (['resultvalue1','resultvalue2', ...]);
// ------------------------------------------------------------------------------

let newElement = document.createElement('p');
newElement.textContent = "DU TEXTE AJOUTE A LA PAGE";
document.body.insertBefore(newElement, document.body.firstChild);