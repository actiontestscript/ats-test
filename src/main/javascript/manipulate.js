// ------------------------------------------------------------------------------
// Get parameter passed by the calling script
// ------------------------------------------------------------------------------
// ex. var param0 = ats_parameter(0);
// ------------------------------------------------------------------------------

// ------------------------------------------------------------------------------
// Use 'ats_element' to get the web element passed by the calling script
// ------------------------------------------------------------------------------
// ex. ats_element.style.visibility = 'hidden';
// ------------------------------------------------------------------------------

console.log('Javascript code launched from ATS execution ...');

// ------------------------------------------------------------------------------
// Return values to the calling script
// ------------------------------------------------------------------------------
// ex. ats_return ('resultvalue');
// or ats_return (['resultvalue1','resultvalue2', ...]);
// ------------------------------------------------------------------------------

console.log(ats_parameter(0));
let idx = ats_parameter(0);
let htmlId = '#skin-client-pref-vector-feature-custom-font-size-value-' + idx;
console.log(htmlId);
document.querySelector(htmlId).click(htmlId);
// Ne fait rien
void 0;

let newElement = document.createElement('p');
newElement.textContent = "Voici le texte ajouté au début de la page: IDX: " + idx;
document.body.insertBefore(newElement, document.body.firstChild);