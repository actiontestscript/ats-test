// ===============================================================================
// Get parameter passed by the calling script
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// var param0 = ats_parameter(0);
// ===============================================================================
// Use 'ats_element' to get the web element passed by the calling script
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ats_element.style.visibility = 'hidden';
// ===============================================================================
// Get iteration values
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ats_iteration
// ats_iterations_count
// ===============================================================================
// Add step comments to the execution report
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ats_comment('my comment in execution report')
// ===============================================================================


// ats_element.style.visibility = 'hidden';
console.log(ats_parameter(0));
ats_return (ats_element);

// ===============================================================================
// Return values to the calling script (single or array value)
// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
// ats_return ('resultvalue');
// ats_return (['resultvalue1','resultvalue2', 42, true, ...]);
// ===============================================================================