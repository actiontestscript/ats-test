# ActionTestScript tests project [![pipeline status](https://gitlab.com/actiontestscript/ats-test/badges/master/pipeline.svg)](https://gitlab.com/actiontestscript/ats-test/-/commits/master)

**com.functional.ats-test (0.0.1)**

ATS project created with Agilitest Editor

# README

---

**This Readme document provides an overview of the technical and functional tests that have been conducted in the ats-test project. It includes details about the testing process, the objectives, and the outcomes of the tests performed.**

### Testing Objectives:

**The primary objectives of the technical and functional tests were as follows:**

- Technical Testing: To verify the performance, reliability, and security aspects of the program under test. This includes testing the system's compatibility, scalability, and robustness. The scripts are short because they only test one main functionality.

- Functional Testing: To ensure that the program under test meets the specified functional requirements and operates correctly in accordance with the defined specifications. The tests are longer because they use different functionalities in one script.

**Then, to execute the tests, they are classified into some groups or execution suites (see execution suites and groups)**

---

### The organization:

**Each test script starts by opening a channel (web, desktop, file explorer, SAP, or API) and a comment that explains the purpose of the test, its prerequisites or expected result, and its last execution date:**

Execution date: **$today - $now**

**Description:**
Trying to close a not open channel

**Expected result:**
Must fail

---

## Functional folder:
Functional testing is used to verify end-to-end usage scenarios or patterns.

- API:

- DeviceChannels:
  These scripts test the behavior of the editor on heavy clients like apps (Telerik, Notepad++, GIMP, VirtualBox), on the Windows desktop, its terminal (Command Prompt), and on its file explorer.

- WebChannels:
  These scripts check that all the paths to perform actions on web browsers can be done correctly with the program (dragNDrop a file, drop a file, filling a form, make a research and buy a product, swipe...).


## Manual folder:
  Tests to be carried out manually before each delivery.


## Technical folder:
Unit testing is a type of software testing in which individual units or components are tested.

- CallWithDataScripts:
  These scripts check if the call of a CSV or JSON is done correctly. For that, the script "OpenChannels" calls a "callWith" script with a data file (.csv or .json) named "openChannel". Then, each "callWith" script runs with a parameter (click on the datafile pathway): indexes 2-4 calls the 2nd row to the 4th, random lines call random rows of the data file, and the parameter one allows calling the rows by their name. The difference with calling with a suite is that if a parameter fails the test, the scripts will stop at this point. With a Suite, the tests would keep on running.

- CallSuiteWithDataScripts:
  To use this principle: click on the datafile pathway, then on the execution suite. It works almost like calling with data; the difference is that if a parameter fails the test, the scripts will keep on running to execute the rest of the steps.

- Channels:
  The tests are made to verify that the opening, closing, or switching of all channels work.

- Device:
  These scripts use the File Explorer and the Desktop channels. The purpose is to verify that the keyboard and mouse functionalities work on both channels (dragNDrop, shortcuts, clicks...).

- MessageBox:
  The program allows the use of a "MessageBox" function (accept or deny). The folder named messagebox contains scripts to test this function.

- Mobile : 
  Scripts to test the mobileStationATS functions.

- SAP:
  This folder contains all the scripts that test the SAP program functions.

- Software:
  This folder contains scripts that show if the script tree is displayed correctly.

- Variables:
  The editor allows creating variables with defined types such as calendar or time values, number values, regex values, password values (encrypted). The purpose of this folder is to verify that they are well executed in each script (and that all the types work).

- Web:
  These scripts check if the program functionalities work well on the web channels (keyboard, clicks, window switching/resizing).

- Must Fail Scripts: In some folders, there exist scripts beginning with a "mustFail" name. These scripts are made to have a negative result (failed) to test functions that have to NOT work.



## Execution suites:
The suites contain tests that will be run by the program terminal.
- all: This suite contains all the scripts that have been written in the ats-test project.
- demo: This is a 2 scripts suite to test quickly the opening of a channel, the url control, and a keyboard functionality.
- heavyClients : Suites that uses programs or prerequisites previously installed on device.
- functionals : All functionnal scripts that can be run on windows. Needs to be run every week before delivery.
- longOne : To test a maximum of parameters and functions of the program. Needs to be run once a day.
- mustFail : Contains all the scripts that must fail.
- techLinux : tests techniques linux validation.
- technicals : All technical scripts than can be run on windows. Needs to be run every week before delivery.
- web: Contains all the scripts that only use light clients such as web browsers. Mainly made to be run on Linux.
- suite : automatically created by the system. 

## Execution groups:
The groups are used as filters during the test execution. If a group is selected (blue button) during the execution of a suite, the program will run the scripts that are common between the suite and the group. If a group is deselected (red button), the scripts that are common between the suite and the group WON'T be run.
- must_fail: These scripts are made to have a negative result (failed) to test functions that have to NOT work.
- SAP
- maintenance
- BUG
- quarantaine

